export class EntityHasUserError extends Error {
    private success: boolean = false
    private data: { error: string }

    constructor(data: { error: string }) {
        super();
        this.success = this.success;
        this.data = data;
    }
}